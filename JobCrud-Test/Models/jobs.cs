﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JobCrud_Test.Models
{
    public class Jobs
    {
        [Key]
        public long Id { get; set; }
        [Required(ErrorMessage = "Enter Job Code")]
        public string Job { get; set; }
        [Required(ErrorMessage = "Enter Job Title")]
        public string JobTitle { get; set; }
        [Required(ErrorMessage = "Enter Description")]
        public string Description { get; set; }

        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Enter Create Date")]
        
        public DateTime CreatedAt { get; set; }

        [DataType(DataType.Date)]        
        [Required(ErrorMessage = "Enter Expire Date")]
        public DateTime ExpiresAt { get; set; }

        public List<Jobs> JobList { get; set; }

    }
}
