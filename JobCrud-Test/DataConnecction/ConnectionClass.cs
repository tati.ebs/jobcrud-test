﻿using System;
using System;
using System.Collections.Generic;
using JobCrud_Test.Models;
using Microsoft.Data.Sqlite;


namespace JobCrud_Test.DataConnecction
{
    public class ConnectionClass
    {
       

        public void CreateDB()
        {
            var connectionStringBuilder = new SqliteConnectionStringBuilder();

            //Use DB in project directory.  If it does not exist, create it:
            connectionStringBuilder.DataSource = "./JobDB.db";

            using (var connection = new SqliteConnection(connectionStringBuilder.ConnectionString))
            {
                connection.Open();
                //Create a table if not exists
                var createTableCmd = connection.CreateCommand();
                createTableCmd.CommandText = "CREATE TABLE IF NOT EXISTS JOB ( Id INTEGER PRIMARY KEY, Job TEXT NOT NULL, JobTitle TEXT NOT NULL, Description TEXT NOT NULL, CreatedAt TEXT NOT NULL, ExpiresAt TEXT NOT NULL )";
                createTableCmd.ExecuteNonQuery();

            }

        
            }

        public void SeedData()
        {
            var connectionStringBuilder = new SqliteConnectionStringBuilder();

            //Use DB in project directory.  If it does not exist, create it:
            connectionStringBuilder.DataSource = "./JobDB.db";

            using (var connection = new SqliteConnection(connectionStringBuilder.ConnectionString))
            {
                connection.Open();
                //Create a table if not exists
                using (var transaction = connection.BeginTransaction())
                {
                    var insertCmd = connection.CreateCommand();

                    insertCmd.CommandText = "insert into JOB (Job,JobTitle, Description,CreatedAt,ExpiresAt) values ('M001','Medical assistants', '" +
                    "Perform many administrative duties, including answering telephones, greeting patients, updating and filing patients medical records among others duties.','12-11-2020','12-11-2025')";
                    insertCmd.ExecuteNonQuery();

                    insertCmd.CommandText = "insert into JOB (Job,JobTitle, Description,CreatedAt,ExpiresAt) values ('M002','Nursing Aide', '" +
                    "Duties listed on a Nursing Aide resume include administering treatments, repositioning bedridden patients, helping with personal care and daily activities, and delivering basic medical care.','12-11-2020','12-11-2025')";
                    insertCmd.ExecuteNonQuery();

                    insertCmd.CommandText = "insert into JOB (Job,JobTitle, Description,CreatedAt,ExpiresAt) values ('M003','Physicians', '" +
                    "Physicians examine patients; take medical histories; prescribe medications; and order, perform, and interpret diagnostic tests.','12-11-2020','12-11-2025')";
                    insertCmd.ExecuteNonQuery();

                    transaction.Commit();
                }

            }


        }


        public List<Jobs> Selectalldata() {

            var connectionStringBuilder = new SqliteConnectionStringBuilder();

            //Use DB in project directory.  If it does not exist, create it:
            connectionStringBuilder.DataSource = "./JobDB.db";

            using (var connection = new SqliteConnection(connectionStringBuilder.ConnectionString))
            {
                //Read the data:
                connection.Open();
                var selectCmd = connection.CreateCommand();
                selectCmd.CommandText = "SELECT * FROM JOB";

                List<Jobs> JobsList = new List<Jobs>();

                using (var reader = selectCmd.ExecuteReader()) 
                {
                    while (reader.Read())
                    {
                        Jobs job = new Jobs();
                        job.Id = (long)reader["Id"];
                        job.Job = (string)reader["Job"];
                        job.JobTitle = (string)reader["JobTitle"];
                        job.Description = (string)reader["Description"];
                        job.CreatedAt = Convert.ToDateTime(reader["CreatedAt"]);
                        job.ExpiresAt = Convert.ToDateTime(reader["ExpiresAt"]);
                        JobsList.Add(job);
                    }
                }

                return JobsList;
            }
                

          

        }

        public Jobs SelectDatabyID(int id)
        {

            var connectionStringBuilder = new SqliteConnectionStringBuilder();

            //Use DB in project directory.  If it does not exist, create it:
            Jobs job = null;
            connectionStringBuilder.DataSource = "./JobDB.db";

            using (var connection = new SqliteConnection(connectionStringBuilder.ConnectionString))
            {
                //Read the data:
                connection.Open();
                SqliteCommand cmd = new SqliteCommand("SELECT * FROM JOB WHERE Id=@id", connection);
                cmd.Parameters.Add(new SqliteParameter("@id", id));


                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        job = new Jobs();
                        job.Id = (long)reader["Id"];
                        job.Job = (string)reader["Job"];
                        job.JobTitle = (string)reader["JobTitle"];
                        job.Description = (string)reader["Description"];
                        job.CreatedAt = Convert.ToDateTime(reader["CreatedAt"]);
                        job.ExpiresAt = Convert.ToDateTime(reader["ExpiresAt"]);
                        
                    }
                }
                return job;

            }


        }


        public int InsertData(Jobs objJob)
        {
            int result = 0;
            try { 
            var connectionStringBuilder = new SqliteConnectionStringBuilder();
           
            //Use DB in project directory.  If it does not exist, create it:
            connectionStringBuilder.DataSource = "./JobDB.db";

            using (var connection = new SqliteConnection(connectionStringBuilder.ConnectionString))
            {
                connection.Open();
                //Create a table if not exists
                using (var transaction = connection.BeginTransaction())
                {
                    var insertCmd = connection.CreateCommand();

                    insertCmd.CommandText = "insert into JOB (Job,JobTitle, Description,CreatedAt,ExpiresAt) values (@Job,@JobTitle, @Description,@CreatedAt,@ExpiresAt)";
                    insertCmd.Parameters.Add(new SqliteParameter("@Job", objJob.Job));
                    insertCmd.Parameters.Add(new SqliteParameter("@JobTitle", objJob.JobTitle ));
                    insertCmd.Parameters.Add(new SqliteParameter("@Description", objJob.Description ));
                    insertCmd.Parameters.Add(new SqliteParameter("@CreatedAt", objJob.CreatedAt ));
                    insertCmd.Parameters.Add(new SqliteParameter("@ExpiresAt", objJob.ExpiresAt ));


                    result = insertCmd.ExecuteNonQuery();



                        transaction.Commit();

                    
                }
                
            }
            
            return result;
            }
            catch (Exception ex)
            { return result =0; }
            

        }

        public int UpdateData(Jobs objJob)
        {
            int result = 0;
            try
            {
                var connectionStringBuilder = new SqliteConnectionStringBuilder();

                //Use DB in project directory.  If it does not exist, create it:
                connectionStringBuilder.DataSource = "./JobDB.db";

                using (var connection = new SqliteConnection(connectionStringBuilder.ConnectionString))
                {
                    connection.Open();
                    //Create a table if not exists
                    using (var transaction = connection.BeginTransaction())
                    {
                        var insertCmd = connection.CreateCommand();

                        insertCmd.CommandText = "UPDATE JOB SET Job=@Job, JobTitle=@JobTitle, Description=@Description,CreatedAt=@CreatedAt,ExpiresAt=@ExpiresAt WHERE Id=@Id";
                        insertCmd.Parameters.Add(new SqliteParameter("@Id", objJob.Id));
                        insertCmd.Parameters.Add(new SqliteParameter("@Job", objJob.Job));
                        insertCmd.Parameters.Add(new SqliteParameter("@JobTitle", objJob.JobTitle));
                        insertCmd.Parameters.Add(new SqliteParameter("@Description", objJob.Description));
                        insertCmd.Parameters.Add(new SqliteParameter("@CreatedAt", objJob.CreatedAt));
                        insertCmd.Parameters.Add(new SqliteParameter("@ExpiresAt", objJob.ExpiresAt));


                        result = insertCmd.ExecuteNonQuery();
                        transaction.Commit();

                    }

                }

                return result;
            }
            catch (Exception ex)
            { return result = 0; }


        }

        public int Delete(Jobs objJob)
        {
            int result = 0;
            try
            {
                var connectionStringBuilder = new SqliteConnectionStringBuilder();

                //Use DB in project directory.  If it does not exist, create it:
                connectionStringBuilder.DataSource = "./JobDB.db";

                using (var connection = new SqliteConnection(connectionStringBuilder.ConnectionString))
                {
                    connection.Open();
                    //Create a table if not exists
                    using (var transaction = connection.BeginTransaction())
                    {
                        var insertCmd = connection.CreateCommand();

                        insertCmd.CommandText = "DELETE FROM JOB WHERE Id=@Id";
                        insertCmd.Parameters.Add(new SqliteParameter("@Id", objJob.Id));                       

                        result = insertCmd.ExecuteNonQuery();
                        transaction.Commit();

                    }

                }

                return result;
            }
            catch (Exception ex)
            { return result = 0; }


        }
    }
}
