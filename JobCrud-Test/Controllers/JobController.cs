﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JobCrud_Test.DataConnecction;
using JobCrud_Test.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JobCrud_Test.Controllers
{
    public class JobController : Controller
    {
        // GET: Job
        public ActionResult Index()
        {
            Jobs job = new Jobs();
            ConnectionClass objDB = new ConnectionClass(); //calling class DBdata
            objDB.CreateDB();
            job.JobList = objDB.Selectalldata();

            if (job.JobList is null) {
                objDB.SeedData();
                job.JobList = objDB.Selectalldata();
            }
            return View(job);
        }

        // GET: Job/Details/5
        public ActionResult Details(int id)
        {
           
            Jobs job = new Jobs();
            ConnectionClass objDB = new ConnectionClass(); //calling class DBdata
            job = objDB.SelectDatabyID(id);
           
            return View(job);
        }

        // GET: Job/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Job/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Jobs objJob)
        {
            try
            {
                if (ModelState.IsValid) //checking model is valid or not
                {
                    ConnectionClass objDB = new ConnectionClass();
                    int result = objDB.InsertData(objJob);
                    
                    ModelState.Clear(); //clearing model
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Error in saving data");
                   return RedirectToAction("Index");
                }

                //return RedirectToAction(nameof(Index));
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        // GET: Job/Edit/5
        public ActionResult Edit(int id)
        {
            Jobs job = new Jobs();
            ConnectionClass objDB = new ConnectionClass(); //calling class DBdata
            job = objDB.SelectDatabyID(id);

            if (job == null)
            {
                return RedirectToAction("Index");
            }
            return View(job);
           
        }

        // POST: Job/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Jobs objJob)
        {
            try
            {
                // TODO: Add update logic here

                if (ModelState.IsValid) //checking model is valid or not
                {
                    ConnectionClass objDB = new ConnectionClass(); //calling class DBdata
                    int result = objDB.UpdateData(objJob);
                    //ViewData["result"] = result;
                    ModelState.Clear(); //clearing model
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Error in saving data");
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        // GET: Job/Delete/5
        public ActionResult Delete(int id)
        {
            Jobs job = new Jobs();
            ConnectionClass objDB = new ConnectionClass(); //calling class DBdata
            job = objDB.SelectDatabyID(id);

            if (job == null)
            {
                return RedirectToAction("Index");
            }
            return View(job);
        }

        // POST: Job/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Jobs objJob)
        {
            try
            {
                ConnectionClass objDB = new ConnectionClass(); //calling class DBdata
                int result = objDB.Delete(objJob);
                ViewData["result"] = result;
                ModelState.Clear(); //clearing model                
                return RedirectToAction("Index");
            }
            catch
            {
                return View(objJob);
            }
        }
    }
}